#! /bin/bash

flatpak install flathub com.visualstudio.code com.obsproject.Studio org.gimp.GIMP org.qbittorrent.qBittorrent com.bitwarden.desktop org.libreoffice.LibreOffice com.anydesk.Anydesk org.kde.kdenlive io.github.mimbrero.WhatsAppDesktop org.videolan.VLC com.brave.Browser org.telegram.desktop com.spotify.Client fr.handbrake.ghb org.blender.Blender org.audacityteam.Audacity
